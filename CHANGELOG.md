# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [4.9.1](https://gitlab.com/tms-elte/frontend-react/compare/v4.9.0...v4.9.1) (2025-01-16)


### Bug Fixes

* Submission card is not shown after the first upload ([66c4275](https://gitlab.com/tms-elte/frontend-react/commit/66c427519ac3ee9cd114743dd8f88af01c61167a))

## [4.9.0](https://gitlab.com/tms-elte/frontend-react/compare/v4.8.1...v4.9.0) (2025-01-13)


### Features

* Add red line to IP Log after last log item in the task availability interval ([a1e01ba](https://gitlab.com/tms-elte/frontend-react/commit/a1e01ba06bb1bd63ad9d7d3a0dffd4444b55ba04))


### Bug Fixes

* Enable showing full error messages to students upon loading an evaluator template ([e72adff](https://gitlab.com/tms-elte/frontend-react/commit/e72adffb1b1a3cdb69fd508520ba12a32857bf44))
* Hide availability and soft deadline for tasks when not defined ([3d35dd9](https://gitlab.com/tms-elte/frontend-react/commit/3d35dd962369fb31bc325836ac239f08ba9acae6))

### [4.8.1](https://gitlab.com/tms-elte/frontend-react/compare/v4.8.0...v4.8.1) (2024-12-23)


### Bug Fixes

* Hungarian translation of Canvas sync levels ([581eed7](https://gitlab.com/tms-elte/frontend-react/commit/581eed785cbd32c79f617a8622dcc17d4c025389))

## [4.8.0](https://gitlab.com/tms-elte/frontend-react/compare/v4.7.0...v4.8.0) (2024-12-16)


### Features

* Add UI for group level notifications ([41e437c](https://gitlab.com/tms-elte/frontend-react/commit/41e437cab0d7469bbf9bedea3e5f7084e7ce53a6))


### Bug Fixes

* Display submission info for entries in the IP log table belonging to the viewed submission ([29be4f3](https://gitlab.com/tms-elte/frontend-react/commit/29be4f3c969a777f28808c0384e5dd3c15775314))
* Fix tooltip on show IP log button ([559ea6d](https://gitlab.com/tms-elte/frontend-react/commit/559ea6da2be0535f784e327d12482889263c8854))

## [4.7.0](https://gitlab.com/tms-elte/frontend-react/compare/v4.6.1...v4.7.0) (2024-12-13)


### Features

* Added IP log viewer for submissions on the instructor panel ([5a960c4](https://gitlab.com/tms-elte/frontend-react/commit/5a960c4a92b8c46bf5528e44b446442f78939702))

### [4.6.1](https://gitlab.com/tms-elte/frontend-react/compare/v4.6.0...v4.6.1) (2024-12-13)


### Bug Fixes

* Readding IP verification checkbox to exit password verification form ([63d3841](https://gitlab.com/tms-elte/frontend-react/commit/63d384115bba93fc5e222d137cd030f8b0dc4d28))

## [4.6.0](https://gitlab.com/tms-elte/frontend-react/compare/v4.5.0...v4.6.0) (2024-12-12)


### Features

* Added scope to notifications ([eed8990](https://gitlab.com/tms-elte/frontend-react/commit/eed89906a6ef4ab850af198b5ab20c3074c14836))
* Display remaining submission time for students ([59cc723](https://gitlab.com/tms-elte/frontend-react/commit/59cc7235057e47e2a556a7b9b4dc55cbfe34de75))
* Display student count on tab header ([2a4b86d](https://gitlab.com/tms-elte/frontend-react/commit/2a4b86d007d876bfe62bccf27662d46d36492c78))
* Entry level password for tasks ([a17b344](https://gitlab.com/tms-elte/frontend-react/commit/a17b3449fb5c05114528e9086820def41aa0a347))
* Handle long task descriptions ([ade1c87](https://gitlab.com/tms-elte/frontend-react/commit/ade1c87f9c1692ac6103b036154af24f597ae2f4))
* Late submissions should be uploadable regardless the upload count ([425927c](https://gitlab.com/tms-elte/frontend-react/commit/425927c6911e97f3b02b2ed0e299dd70fd19928f))
* Partial Canvas synchronization ([35b278d](https://gitlab.com/tms-elte/frontend-react/commit/35b278d41e3a874c188b3496f3315e77bf4198e6))
* Show icon on left sidebar for Canvas groups ([7cbe9c2](https://gitlab.com/tms-elte/frontend-react/commit/7cbe9c2c96a2180d2bfb1eba08ed7195aebf33fd))
* Specify file name in confirmation dialog when deleting Task File ([e514b8f](https://gitlab.com/tms-elte/frontend-react/commit/e514b8fc87c6b063fb359f6195d325a0f52b0071))

## [4.5.0](https://gitlab.com/tms-elte/frontend-react/compare/v4.4.0...v4.5.0) (2024-11-19)


### Features

* Added configuration for restrictable number of submission attempts ([55182d3](https://gitlab.com/tms-elte/frontend-react/commit/55182d39acb630060e4f93f085c78cd952c08be9))
* Show directories before files in code browser ([6f115d7](https://gitlab.com/tms-elte/frontend-react/commit/6f115d7e7165b5fc1f22399e51927ac0bb842acd))

## [4.4.0](https://gitlab.com/tms-elte/frontend-react/compare/v4.3.0...v4.4.0) (2024-11-10)


### Features

* Assigning a lecturer is mandatory when creating a new course ([24caad0](https://gitlab.com/tms-elte/frontend-react/commit/24caad075c76588213f2fcc5d25563243bc9de1c))
* Create Docker image for frontend ([fc4f71c](https://gitlab.com/tms-elte/frontend-react/commit/fc4f71c19bc0396d614b2e2c664b01c3c4c625a7))

## [4.3.0](https://gitlab.com/tms-elte/frontend-react/compare/v4.2.1...v4.3.0) (2024-10-29)


### Features

* Rename all Neptun code occurrences to User code ([62c2c83](https://gitlab.com/tms-elte/frontend-react/commit/62c2c83e623057f85b25b645a8f81979265f4317))
* Set the default submission view based on the screen size ([8f3d79f](https://gitlab.com/tms-elte/frontend-react/commit/8f3d79fdb96b9eb65643c44d7bce2da9b8b3032c))
* System statistics administrative interface  ([a14cf47](https://gitlab.com/tms-elte/frontend-react/commit/a14cf47423acfc0e066628b3c36950be1ec9c947))


### Bug Fixes

* Admins should be able to add instructors to group ([d9a48a1](https://gitlab.com/tms-elte/frontend-react/commit/d9a48a1d3ad0d32e85afa08577a24e86d1901f34))
* Web application tester default run time can be out of valid interval ([2373110](https://gitlab.com/tms-elte/frontend-react/commit/23731103352b44992b862991d1b866f1fbd23452))

### [4.2.1](https://gitlab.com/tms-elte/frontend-react/compare/v4.2.0...v4.2.1) (2024-10-08)


### Bug Fixes

* Downgrading remark-gfm to v3, as v4 is not working correctly ([8a5360d](https://gitlab.com/tms-elte/frontend-react/commit/8a5360dfef14a680af744aedf0c202d225d03ed4))
* Improved regex validation of Neptun codes ([1606bd7](https://gitlab.com/tms-elte/frontend-react/commit/1606bd70398e988941566e7c1d2324f0c3f671db))

## [4.2.0](https://gitlab.com/tms-elte/frontend-react/compare/v4.1.0...v4.2.0) (2024-10-07)


### Features

* Added frontend validation rules for course codes ([8f79f5f](https://gitlab.com/tms-elte/frontend-react/commit/8f79f5fb9d0b2fe2b4f00525545683bb0759cd1e))
* Show list of conditions to sync from Canvas ([0bc47f8](https://gitlab.com/tms-elte/frontend-react/commit/0bc47f85b212e72c2f993a6cffd2f7efd24581ec))
* Upload custom files for assignments ([ebed39c](https://gitlab.com/tms-elte/frontend-react/commit/ebed39c000a40a5858de226650d9d71684c92133))

## [4.1.0](https://gitlab.com/tms-elte/frontend-react/compare/v4.0.2...v4.1.0) (2024-10-07)


### Features

* Added Frontend and Backend version to About page ([709efe2](https://gitlab.com/tms-elte/frontend-react/commit/709efe21f910268c1ae57e80c18a2b1034eaf40f))
* Support LaTeX formulas in markdown descriptions ([f12c74c](https://gitlab.com/tms-elte/frontend-react/commit/f12c74c739ac6a459639e0f265aad1c2d888eade))

### [4.0.2](https://gitlab.com/tms-elte/frontend-react/compare/v4.0.1...v4.0.2) (2024-06-21)


### Bug Fixes

* Turn off password save option in browsers for submission password verification box ([66d44fb](https://gitlab.com/tms-elte/frontend-react/commit/66d44fb74057f110e41522b4d1c140086867e5df))

### [4.0.1](https://gitlab.com/tms-elte/frontend-react/compare/v4.0.0...v4.0.1) (2024-06-21)


### Bug Fixes

* Turn off password save option in browsers for submission password verification box ([c1db0ad](https://gitlab.com/tms-elte/frontend-react/commit/c1db0adb8d2dbe3b4ad067b3ccf5f1afbb3fcf8b))

## [4.0.0](https://gitlab.com/tms-elte/frontend-react/compare/v3.7.0...v4.0.0) (2024-06-20)


### Features

* Add about page ([e08882d](https://gitlab.com/tms-elte/frontend-react/commit/e08882d7add0aaa74cc18258a7ec03031ba24861))
* Add multiple course codes to courses ([3da8e82](https://gitlab.com/tms-elte/frontend-react/commit/3da8e8292bd3d9890f1584f1c5e250bfb72c3d22))
* Add periodic refetch to assignment evaluation ([bc41c2d](https://gitlab.com/tms-elte/frontend-react/commit/bc41c2d883866a430020fbb6d2257a268282d75b))
* Handle misconfigured system clock on client machine ([f064a50](https://gitlab.com/tms-elte/frontend-react/commit/f064a507630ca066534bd979365693a56d22d94e))
* Integrate file and code browser for submissions ([f576b49](https://gitlab.com/tms-elte/frontend-react/commit/f576b49b783c894b8d2b47723003d5191de67593))


### Bug Fixes

* Invalidate query on correct answer submission ([3a5d1d1](https://gitlab.com/tms-elte/frontend-react/commit/3a5d1d1e40191c378faa168acf6c3b7f6829ae65))
* IP address list is not present on all interfaces  ([2d8285d](https://gitlab.com/tms-elte/frontend-react/commit/2d8285da261d04c07725ca8b8f980e80658e0fa9))

## [3.7.0](https://gitlab.com/tms-elte/frontend-react/compare/v3.6.1...v3.7.0) (2024-04-11)


### Features

* Display IP log for submissions ([14d0973](https://gitlab.com/tms-elte/frontend-react/commit/14d09737f12c77c4ed4694bb5821e1d29b9f3b7a))
* Do not show Add Group button for instructors ([33b16aa](https://gitlab.com/tms-elte/frontend-react/commit/33b16aae13e3c0f0d8bc94446a89e44218864a4e))
* Introduced support for runtime configurable environment variables ([95ce22b](https://gitlab.com/tms-elte/frontend-react/commit/95ce22b3fec8bf537f4abfb019c2c46d30b086f1))
* Regular tasks for Canvas synchronized groups ([9680fb3](https://gitlab.com/tms-elte/frontend-react/commit/9680fb3a5a3ae7d973d0f8359cc7558622daf9da))
* Student initiated Canvas synchronization ([2213310](https://gitlab.com/tms-elte/frontend-react/commit/2213310af211c5e50d31cefe1276edb0eac54037))

### [3.6.1](https://gitlab.com/tms-elte/frontend-react/compare/v3.6.0...v3.6.1) (2024-03-14)


### Bug Fixes

* Main content is overlapping sidebar on certain display sizes ([382d2fb](https://gitlab.com/tms-elte/frontend-react/commit/382d2fb528f5bde10d433dd634ee423adfe1565b))
* Set sidebar height to dynamic ([5c80365](https://gitlab.com/tms-elte/frontend-react/commit/5c80365c56588d587e9ecb5fca2ef28b8c6d79ea))

## [3.6.0](https://gitlab.com/tms-elte/frontend-react/compare/v3.5.1...v3.6.0) (2024-02-13)


### Features

* Add user selection feature with autocomplete support based on name and student code ([fa97ae8](https://gitlab.com/tms-elte/frontend-react/commit/fa97ae808409f7e6c80e98b0652232594c82f066))

### [3.5.1](https://gitlab.com/tms-elte/frontend-react/compare/v3.5.0...v3.5.1) (2024-02-08)


### Bug Fixes

* Increase sidebar width. ([645a2cd](https://gitlab.com/tms-elte/frontend-react/commit/645a2cdbcd1b15b111fcf05111f27cdb4defe05b))

## [3.5.0](https://gitlab.com/tms-elte/frontend-react/compare/v3.4.0...v3.5.0) (2024-02-05)


### Features

* Notification management on the UI ([83df3d0](https://gitlab.com/tms-elte/frontend-react/commit/83df3d047bf97d2f3f8b359e619928726bac814c))

## [3.4.0](https://gitlab.com/tms-elte/frontend-react/compare/v3.3.1...v3.4.0) (2024-01-03)


### Features

* Added automated testing for web applications ([f757738](https://gitlab.com/tms-elte/frontend-react/commit/f757738888edc9350a193cf1e9e473178a8877b5))


### Bug Fixes

* Set default plagiarism type on new plagiarism form ([44682ce](https://gitlab.com/tms-elte/frontend-react/commit/44682ce538e9e349dd4c069341b5e9b86d4ada8d))

### [3.3.1](https://gitlab.com/tms-elte/frontend-react/compare/v3.3.0...v3.3.1) (2023-10-23)


### Bug Fixes

* Disabled buttons while mutations are loading ([cc1fe8c](https://gitlab.com/tms-elte/frontend-react/commit/cc1fe8ccf0c822395a9bfb1e2b2bdad4d1a28ef5))
* Do not show Add Instructors panel for instructors with insufficient privilege ([032a477](https://gitlab.com/tms-elte/frontend-react/commit/032a47750f9982ae6d51bd5ea4170a36994af8ad))
* Group number validation message is shown twice on the new/edit group form ([35ca56f](https://gitlab.com/tms-elte/frontend-react/commit/35ca56f3c905b013a31b7bd851f8a667185ff982))
* Page load issue after grading ([8b39b1a](https://gitlab.com/tms-elte/frontend-react/commit/8b39b1a01bfcd1502fcfb002ed46dcb114d5f80c))

## [3.3.0](https://gitlab.com/tms-elte/frontend-react/compare/v3.2.1...v3.3.0) (2023-09-17)


### Features

* Filterable group list for instructors ([cbf2361](https://gitlab.com/tms-elte/frontend-react/commit/cbf2361d70233c8f4ed0f77787feae469961e008))
* Handle no-submission state for student assignments ([d7fce15](https://gitlab.com/tms-elte/frontend-react/commit/d7fce15b6a9a4e9edbea009f335bbc2abc0f5822))
* Handle partially successful Canvas synchronization ([9180032](https://gitlab.com/tms-elte/frontend-react/commit/9180032b9905f31b3cc064aa9ce83a72f9dcb54a))
* Reporting results for all test cases ([f374eb3](https://gitlab.com/tms-elte/frontend-react/commit/f374eb34808d5759ea59d2c02a23c7e4842ed543))


### Bug Fixes

* Static analyzer results disappear after grading until the next page refresh ([bf811f9](https://gitlab.com/tms-elte/frontend-react/commit/bf811f91422d5dbbb964f41fcec21956ceefa6bb))

### [3.2.1](https://gitlab.com/tms-elte/frontend-react/compare/v3.2.0...v3.2.1) (2023-06-04)


### Bug Fixes

* Docker image build or pull date is not updated after image updated ([1c99817](https://gitlab.com/tms-elte/frontend-react/commit/1c998174b3ed52faba9aebf77d8e79b8fb18dea5))
* Exam group checkbox is not set correctly on group form ([ae53505](https://gitlab.com/tms-elte/frontend-react/commit/ae5350577230467b9c52bb1f0713ea9f1058d401))
* Selected semester is null for students and instructors after page refresh ([21df929](https://gitlab.com/tms-elte/frontend-react/commit/21df92962935afd52a005a5c7c744c77666a3e60))

## [3.2.0](https://gitlab.com/tms-elte/frontend-react/compare/v3.1.0...v3.2.0) (2023-05-26)


### Features

* Integrate JPlag plagiarism detection tool ([bb1fd4f](https://gitlab.com/tms-elte/frontend-react/commit/bb1fd4f194caeee3e4d972f21fcd8f0fc0e2fe50))

## [3.1.0](https://gitlab.com/tms-elte/frontend-react/compare/v3.0.0...v3.1.0) (2023-04-27)


### Features

* Added alternative UI theme to distinguish multiple TMS instances visually ([e1df152](https://gitlab.com/tms-elte/frontend-react/commit/e1df152c650ed8e866fc5719ccd5eeed6af08254))
* Added Google Analytics support ([944a49f](https://gitlab.com/tms-elte/frontend-react/commit/944a49f09254c7ffebc459896987cdab8ab7608b))
* Ask for confirmation before adding a new semester ([4ffd793](https://gitlab.com/tms-elte/frontend-react/commit/4ffd79325962db063624ff41c3929cda103812e8))
* Display last Canvas synchronization timestamp ([95abdfc](https://gitlab.com/tms-elte/frontend-react/commit/95abdfceb196c59ee1adcabbbb0b9fde9db2c047))
* Hide Group Time if same as Local Time ([3592fb5](https://gitlab.com/tms-elte/frontend-react/commit/3592fb51d87178c2dc2607bc5be5722569b67d1a))
* Show current student's name and Neptun code on the Add Notes modal ([4fed934](https://gitlab.com/tms-elte/frontend-react/commit/4fed934e10f49012bd1ebfa7363df48659319118))

## [3.0.0](https://gitlab.com/tms-elte/frontend-react/compare/v2.10.0...v3.0.0) (2023-03-29)


### Features

* New evaluator settings UI ([756fbe1](https://gitlab.com/tms-elte/frontend-react/commit/756fbe114a42c5b83c14dd97aa4486c9f3448a6f))
* UI for static analyzer settings ([7b75d73](https://gitlab.com/tms-elte/frontend-react/commit/7b75d737d560cecf9b261c1798ba52ef55a70009))

## [2.10.0](https://gitlab.com/tms-elte/frontend-react/compare/v2.9.1...v2.10.0) (2023-03-06)


### Features

* Implement confirm close functionality for all modals with forms ([6cc70d6](https://gitlab.com/tms-elte/frontend-react/commit/6cc70d6f8c4b0afa3d15002b2616a2bfe646e39c))
* Import multiple test cases from CSV and XLS files ([d1e89e6](https://gitlab.com/tms-elte/frontend-react/commit/d1e89e6c0779d605068348fc3091889037d25e1f))
* user interface for task-level git repositories ([ea25c99](https://gitlab.com/tms-elte/frontend-react/commit/ea25c9940e8111b74fa2084f0ee97920efacb8e3))

### [2.9.1](https://gitlab.com/tms-elte/frontend-react/compare/v2.9.0...v2.9.1) (2023-02-10)


### Bug Fixes

* Hide previous error messages after successful upload ([bcc6525](https://gitlab.com/tms-elte/frontend-react/commit/bcc652501f8116be41e623c4203779b726a9fd4f))

## [2.9.0](https://gitlab.com/tms-elte/frontend-react/compare/v2.8.2...v2.9.0) (2023-02-07)


### Features

* Always show students' names and Neptun codes on the grid view ([6c8dd88](https://gitlab.com/tms-elte/frontend-react/commit/6c8dd888c8129ed4f34e2f523b864f8666364475))
* Plagiarism basefile support ([163769d](https://gitlab.com/tms-elte/frontend-react/commit/163769de709a0c0cb8b6ad0317068f64e108de80))
* User Interface for personal notes ([51ceaf2](https://gitlab.com/tms-elte/frontend-react/commit/51ceaf22d09fef25c214c946d289e163a6a83e62))


### Bug Fixes

* Build errors after the React Hook From package is updated ([2bc3e03](https://gitlab.com/tms-elte/frontend-react/commit/2bc3e0360e3e515406834af1ba72a9fceca99db9))

### [2.8.2](https://gitlab.com/tms-elte/frontend-react/compare/v2.8.1...v2.8.2) (2022-11-11)


### Bug Fixes

* Disable login button while waiting for response and add timeout after failure ([430096f](https://gitlab.com/tms-elte/frontend-react/commit/430096f1271ba2ed232415fdcd048eade6902361))
* Fix displaying solutions section and instructor files ([d08b50b](https://gitlab.com/tms-elte/frontend-react/commit/d08b50bea8449e960e1681938aa00e633188c64c))

### [2.8.1](https://gitlab.com/tms-elte/frontend-react/compare/v2.8.0...v2.8.1) (2022-10-21)


### Bug Fixes

* GraderModal confirm exit dialog doesn't always show ([1faba1b](https://gitlab.com/tms-elte/frontend-react/commit/1faba1bc4014ac3896f2e1cdea1ad544fc409bdf))

## [2.8.0](https://gitlab.com/tms-elte/frontend-react/compare/v2.7.2...v2.8.0) (2022-10-21)


### Features

* Query upload file size limits from the backend ([7632696](https://gitlab.com/tms-elte/frontend-react/commit/76326962a422930e65e46469422b5b2891f62c65))


### Bug Fixes

* Grading window closes too easily ([d39c8bd](https://gitlab.com/tms-elte/frontend-react/commit/d39c8bd94034a0bc74e21ba22d49b7c8d9bd6e20))
* Make checkbox labels on mock login clickable ([4bdfbaf](https://gitlab.com/tms-elte/frontend-react/commit/4bdfbaf666e0d3fa48df126f2ba7e138400e678a))

### [2.7.2](https://gitlab.com/tms-elte/frontend-react/compare/v2.7.1...v2.7.2) (2022-09-19)


### Bug Fixes

* Properly save default submission status when grading ([835eac4](https://gitlab.com/tms-elte/frontend-react/commit/835eac44cfd60049f05cdd0404278a53b4543db5))

### [2.7.1](https://gitlab.com/tms-elte/frontend-react/compare/v2.7.0...v2.7.1) (2022-09-19)


### Bug Fixes

* Correct status selected when regrading submissions ([c7eecb5](https://gitlab.com/tms-elte/frontend-react/commit/c7eecb5c80bc6133e13f8a81f1dee76c5bc641ac))
* Disable caching for index.html on Apache webservers ([2e5c635](https://gitlab.com/tms-elte/frontend-react/commit/2e5c635885cda6f69bd91f838dcb8d3242810138))

## [2.7.0](https://gitlab.com/tms-elte/frontend-react/compare/v2.6.0...v2.7.0) (2022-05-15)


### Features

* Added easier mock login with predefined users ([059a191](https://gitlab.com/tms-elte/frontend-react/commit/059a1911e7ec22cdc5065174c1aff3be04f66d82))
* CodeCompass integration ([83deaaf](https://gitlab.com/tms-elte/frontend-react/commit/83deaaf6030c8adb865ece8384339678db69c8a9))
* Inform students about how to upload new solutions to a Canvas tasks ([cea8a36](https://gitlab.com/tms-elte/frontend-react/commit/cea8a368901c0207e5016a6e340025a27b18eef0))
* Password protected task user interface ([b450b92](https://gitlab.com/tms-elte/frontend-react/commit/b450b929b17e377d700a84c1f0afc5bda3a75290))
* Remote execution of web applications ([a379676](https://gitlab.com/tms-elte/frontend-react/commit/a379676280daa02e516319bb1e4913ec303bca8d))
* Show Canvas URL for groups and tasks ([95f4148](https://gitlab.com/tms-elte/frontend-react/commit/95f4148068d7a4b864bd646ea64639b193d6dbb7))

## [2.6.0](https://gitlab.com/tms-elte/frontend-react/compare/v2.5.0...v2.6.0) (2022-04-14)


### Features

* Added option to reevaluate ungraded submissions when modifying automated testing ([d52128f](https://gitlab.com/tms-elte/frontend-react/commit/d52128f2d50c0ba939dae4f05e232664958d5490))
* Display TMS version information and repository URL on bottom of the login page ([017eb70](https://gitlab.com/tms-elte/frontend-react/commit/017eb70ad3e1fc065c32cbed6848bf6b113f14f8))
* Display upload count for solutions ([15355fb](https://gitlab.com/tms-elte/frontend-react/commit/15355fb6d418845d5eb473e5d5ab547445e0090a))
* Optional sorting of student submissions ([efacf07](https://gitlab.com/tms-elte/frontend-react/commit/efacf07803562691e9a34994cf19428d0e3d8575))


### Bug Fixes

* Disabled test creation for question sets without groups in the active semester ([72a103e](https://gitlab.com/tms-elte/frontend-react/commit/72a103e3cdafb5e97afed8038a5fa48a0e2e8e0a))

## [2.5.0](https://gitlab.com/tms-elte/frontend-react/compare/v2.4.0...v2.5.0) (2022-03-28)


### Features

* Display local plagiarism results ([17eabc4](https://gitlab.com/tms-elte/frontend-react/commit/17eabc44b80387cd90e2976a69f99c0c80019ffa))
* Enable docker image update of auto tester ([d7ceeeb](https://gitlab.com/tms-elte/frontend-react/commit/d7ceeeb631a503e08f6d22e52412f65842b3750d))
* Grid view of tasks ([1195ccb](https://gitlab.com/tms-elte/frontend-react/commit/1195ccb0664c8e0ed8d169f639f604efcc101840))
* Sort students by their name and university identifier ([2644f16](https://gitlab.com/tms-elte/frontend-react/commit/2644f16f346fea8039cf740fa639ace5303ca5cc))

## [2.4.0](https://gitlab.com/tms-elte/frontend-react/compare/v2.3.0...v2.4.0) (2022-03-22)


### Features

* Check backend version requirement ([f81ca7f](https://gitlab.com/tms-elte/frontend-react/commit/f81ca7fc55b323583b8f9a673de29d7d77acbe51))
* Default status for Failed submissions is Rejected ([601dcf6](https://gitlab.com/tms-elte/frontend-react/commit/601dcf6f512a4a1ff23abad1987a003c5242ea2d))
* File upload support for automatic tester ([9e5cb22](https://gitlab.com/tms-elte/frontend-react/commit/9e5cb2226d2e67e7ba2d5a19bf0e082c18df50dd))


### Bug Fixes

* Activating the automated tester resets the task deadlines ([5729574](https://gitlab.com/tms-elte/frontend-react/commit/57295743a6cef06ef8ad5dc01527c8f80c212478))
* Misleading error message upon oversized file upload ([faa4b26](https://gitlab.com/tms-elte/frontend-react/commit/faa4b266eb1b75dfe23b4f8f42ceea3891148dac))

## [2.3.0](https://gitlab.com/tms-elte/frontend-react/compare/v2.2.0...v2.3.0) (2022-02-06)


### Features

* Command line arguments input for test cases ([4dfdcdf](https://gitlab.com/tms-elte/frontend-react/commit/4dfdcdf9ad3a91e2a2291e354c1d720a666bf2ee))

## [2.2.0](https://gitlab.com/tms-elte/frontend-react/compare/v2.1.0...v2.2.0) (2021-12-07)


### Features

* Add user settings page ([490b143](https://gitlab.com/tms-elte/frontend-react/commit/490b143d7eaf69e07280d606b8d7ed383f6144af))
* Display finalized future exams ([b7341d0](https://gitlab.com/tms-elte/frontend-react/commit/b7341d0051686847fcd181d5c510154ace16d5b5))
* Sort student submissions by grading and date ([56fd09c](https://gitlab.com/tms-elte/frontend-react/commit/56fd09cf044c6d2a470b747bd99673e9a41c1105))

## [2.1.0](https://gitlab.com/tms-elte/frontend-react/compare/v2.0.3...v2.1.0) (2021-10-27)


### Features

* Add a Markdown editor on the instructor interface for the task description ([80831b6](https://gitlab.com/tms-elte/frontend-react/commit/80831b6c91021a2dbadfd68c2dc997befd8c3185))
* Add insert table command to markdown editor ([3d1ef00](https://gitlab.com/tms-elte/frontend-react/commit/3d1ef00739001c2e15880c411d4376ba63f76bfd))
* Add support for GitHub Flavoured Markdown ([4a39ea8](https://gitlab.com/tms-elte/frontend-react/commit/4a39ea8d970fcac48436b722ee77de4f5b046d5d))
* Use Luxon and manage timezones ([075489f](https://gitlab.com/tms-elte/frontend-react/commit/075489fb5889facf9872bfeba5b0136283776f15))


### Bug Fixes

* Automated evaluator result not shown for accepted and rejected submissions ([ea3b2dd](https://gitlab.com/tms-elte/frontend-react/commit/ea3b2ddf97456917cf6bfc26024537774ba4cdd4))

### [2.0.3](https://gitlab.com/tms-elte/frontend-react/compare/v2.0.2...v2.0.3) (2021-10-23)


### Bug Fixes

* Cleanup intervals and timeouts in TestWriterPage.tsx correctly ([6f00de8](https://gitlab.com/tms-elte/frontend-react/commit/6f00de8a02e5925fb3f75b41ff3ce3f4b1a78c4f))
* Display multiline plagiarism notes correctly ([63773a0](https://gitlab.com/tms-elte/frontend-react/commit/63773a03deafb249789fde0e8dd708f3a97cf4d2))
* Display multiline submission notes correctly ([6afc292](https://gitlab.com/tms-elte/frontend-react/commit/6afc292cc0de06694f3096e4772613b6e7eee63c))
* QuestionFormModal nad AnswerFormModal components should clear form fields when the user adds a new question or answer ([04104d8](https://gitlab.com/tms-elte/frontend-react/commit/04104d824a0a7604c743659e4311079a54eb91ac))
* Regular expressions are not escaped in DualListBoxControl.tsx ([2335b10](https://gitlab.com/tms-elte/frontend-react/commit/2335b102ef58ddc27aa49358907da7e2bad6b202))
* Translation issue on dynamic content after page reload ([63b13b1](https://gitlab.com/tms-elte/frontend-react/commit/63b13b151bdfd7181d6c72976e3105360073caf4))

### [2.0.2](https://gitlab.com/tms-elte/frontend-react/compare/v2.0.1...v2.0.2) (2021-09-28)


### Bug Fixes

* Show the file upload form for tasks where the submission is in late submission status ([d9666c4](https://gitlab.com/tms-elte/frontend-react/commit/d9666c413be01e6bf8c0eb9d21a1ccdef0c66c2f), [b8ea521](https://gitlab.com/tms-elte/frontend-react/commit/b8ea52154a9705e53680ce8511cc1bb751d7150a))

## 2.0.0 (2021-09-22)


Initial public release.
