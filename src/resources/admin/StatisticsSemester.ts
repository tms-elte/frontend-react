export interface StatisticsSemester {
    groupsCount: number;
    tasksCount: number;
    submissionsCount: number;
    testedSubmissionCount: number;
}
