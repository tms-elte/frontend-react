export interface Statistics {
    groupsCount: number;
    tasksCount: number;
    submissionsCount: number;
    testedSubmissionCount: number;
    submissionsUnderTestingCount: number;
    submissionsToBeTested: number;
    diskFree?: number;
}
