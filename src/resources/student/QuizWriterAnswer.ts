export interface QuizWriterAnswer {
    id: number;
    text: string;
}
