export interface VerifyItem {
    id: number;
    password: string;
    disableIpCheck: boolean;
}
