export interface QuizResultQuestion {
    questionID: number;
    questionText: string;
    isCorrect: boolean;
    answerText: string;
}
