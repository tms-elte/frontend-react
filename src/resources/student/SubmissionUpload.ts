export interface SubmissionUpload {
    taskID: number;
    file: File;
}
