export interface BaseFileUpload {
    courseID: number;
    files: File[];
}
