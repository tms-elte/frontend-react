export interface SetupEvaluatorEnvironment {
    testOS: string;
    imageName?: string;
    files: FileList | null;
}
