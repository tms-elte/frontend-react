export interface QuizQuestion {
    id: number;
    text: string;
    questionsetID: number;
}
