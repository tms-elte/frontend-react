export interface QuizAnswer {
    id: number;
    text: string;
    correct: number;
    questionID: number;
}
