export interface CodeCompassParameters {
    packagesInstallInstructions: string;
    compileInstructions: string;
}
