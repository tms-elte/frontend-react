export interface TestCase {
    id: number;
    arguments: string;
    input: string;
    output: string;
    taskID: number;
}
