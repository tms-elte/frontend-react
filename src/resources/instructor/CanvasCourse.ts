export interface CanvasCourse {
    id: number;
    name: string;
}
