export interface CanvasSection {
    id: number;
    name: string;
    totalStudents: number | null | undefined;
}
