export interface CanvasOauth2Response {
    code: string | null,
    state: string | null,
    error: string | null
}
