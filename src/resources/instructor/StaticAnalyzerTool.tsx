export interface StaticAnalyzerTool {
    name: string;
    title: string;
    outputPath: string;
}
