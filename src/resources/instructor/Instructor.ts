export interface Instructor {
    id: number;
    name: string;
    userCode: string;
}
