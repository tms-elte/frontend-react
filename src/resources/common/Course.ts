export interface Course {
    id: number;
    name: string;
    codes: string[];
    lecturerNames: string[];
}
