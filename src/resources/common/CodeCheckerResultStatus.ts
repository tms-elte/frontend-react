export type CodeCheckerResultStatus = 'In Progress' | 'No Issues' | 'Issues Found' | 'Analysis Failed' | 'Runner Error';
