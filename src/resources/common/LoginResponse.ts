export interface LoginResponse {
    accessToken: string;
    imageToken: string;
}
