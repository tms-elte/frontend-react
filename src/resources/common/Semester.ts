export interface Semester {
    id: number;
    name: string;
    actual: number;
}
