export interface User {
    id: number;
    userCode: string;
    name?: string;
}
