export interface ServerErrorResponse {
    name: string;
    message: string;
    code: number;
    status: number;
}
