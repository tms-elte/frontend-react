export interface TaskFile {
    id: number;
    name: string;
    uploadTime: string;
}
