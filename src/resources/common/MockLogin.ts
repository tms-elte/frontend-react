export interface MockLogin {
    userCode: string;
    name: string;
    email: string;
    isStudent: boolean;
    isTeacher: boolean;
    isAdmin: boolean;
}
