export interface AutoTesterResult {
    testCaseNr: number;
    isPassed: boolean;
    errorMsg: string;
}
