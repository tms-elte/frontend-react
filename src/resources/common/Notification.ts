export interface Notification {
    id: number;
    message: string;
    dismissible: boolean;
    isGroupNotification: boolean;
}
