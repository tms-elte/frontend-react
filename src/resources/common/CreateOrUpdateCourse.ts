export interface CreateOrUpdateCourse {
    name: string;
    codes: string[];
    lecturerUserCodes?: string[];
}
