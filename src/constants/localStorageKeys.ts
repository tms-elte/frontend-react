export const ACCESS_TOKEN_LOCAL_STORAGE_KEY = 'accessToken';
export const IMAGE_TOKEN_LOCAL_STORAGE_KEY = 'imageToken';
export const INSTRUCTOR_TASK_VIEW_LOCAL_STORAGE_KEY = 'instructorTaskView';
export const INSTRUCTOR_GROUP_VIEW_LOCAL_STORAGE_KEY = 'instructorGroupView';
export const PROXY_AUTH_REDIRECT_LOCAL_STORAGE_KEY = 'proxyAuthRedirect';
export const DISMISSED_NOTIFICATIONS_LOCAL_STORAGE_KEY = 'dismissedNotifications';
