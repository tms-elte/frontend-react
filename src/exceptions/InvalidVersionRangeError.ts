/**
 * Indicates if the given version number does not meet with the required version range
 */
export class InvalidVersionRangeError extends Error {}
